"use strict";

var verticalUnit = getUnit("--unit--vertical");
function getUnit(id) {
  var remFactor = 16;
  var rawUnit = getComputedStyle(document.documentElement).getPropertyValue(id) || "1.7rem";
  var remUnit = parseFloat(rawUnit);
  var pxUnit = remUnit * remFactor;
  return pxUnit;
}

// Throttle found here : https://gist.github.com/ionurboz/51b505ee3281cd713747b4a84d69f434
function throttle(func, wait, options) {
  var context, args, result;
  var timeout = null;
  var previous = 0;
  if (!options) options = {};
  var later = function later() {
    previous = options.leading === false ? 0 : Date.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function () {
    var now = Date.now();
    if (!previous && options.leading === false) previous = now;
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
}
function setWindowHeightFactor() {
  var windowHeight = window.innerHeight;
  var min = 650;
  var delta = windowHeight - min;
  var factor = roundToNearestHalf(delta / 300) + 1;
  document.querySelector(":root").style.setProperty("--window-height-factor", factor);
}
function roundToNearestHalf(num) {
  var round = Math.round(num * 2) / 2;
  return Math.max(round, 0);
}
function toggleLogoState() {
  var scrollY = window.scrollY || window.pageYOffset;
  if (scrollY > 10) {
    document.querySelector("#main-header").classList.add("minimized");
  } else {
    document.querySelector("#main-header").classList.remove("minimized");
  }
}
function toggleFooterState() {
  if (scrollY > 90) {
    document.querySelector("#main-footer").classList.add("main-footer--background");
  } else {
    document.querySelector("#main-footer").classList.remove("main-footer--background");
  }
}
function fixFootNotes() {
  var footnotes = document.querySelectorAll('a[href^="#sdfootnote"]');
  footnotes.forEach(function (footnote) {
    var href = footnote.href;
    footnote.classList.add("footnote");
    if (href.includes("sym")) {
      footnote.id = footnote.hash.replace("sym", "anc").replace("#", "");
    } else if (href.includes("anc")) {
      footnote.id = footnote.hash.replace("anc", "sym").replace("#", "");
    }
  });
}
function removeAccents(str) {
  var from = "áäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
  var to = "aaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }
  return str;
}
function slugify(str) {
  return removeAccents(str.toLowerCase());
}
function subscribe(event) {
  event.preventDefault();
  var emailInput = document.querySelector("#subscribe-form input");
  if (emailInput.value.toLowerCase().match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
    var header = {
      method: "POST",
      body: JSON.stringify(emailInput.value)
    };
    fetch("/subscribe.json", header).then(function (res) {
      return res.json();
    }).then(function (data) {
      var formNode = emailInput.parentNode.parentNode;
      formNode.outerHTML = "<p>" + data.message + "</p>";
    });
  } else {
    emailInput.value = "E-mail invalide. Recommencez.";
  }
}
var panelNav = document.querySelector(".panel");
var navOverlay = document.querySelector("#nav-overlay");
var openNavBtn = document.querySelector("button.open-nav");
var closeNavBtn = document.querySelector(".panel-close");
function closeNav() {
  panelNav.classList.remove("panel--visible");
  navOverlay.classList.remove("nav-overlay--visible");
  document.body.classList.remove("no-scroll");
}
document.addEventListener("DOMContentLoaded", function () {
  ragadjust("h1, h2, h4, h5", ["all"]);
  window.window.scrollTo({
    top: 0
  });
  var handleScroll = throttle(function () {
    toggleLogoState();
    if (window.innerWidth <= 680) {
      toggleFooterState();
    }
  }, 100);
  window.addEventListener("scroll", handleScroll);
  setWindowHeightFactor();
  window.addEventListener("resize", function () {
    setWindowHeightFactor();
  });
  fixFootNotes();
  window.addEventListener("keyup", function (event) {
    if (event.key === "Escape") {
      closeNav();
    }
  });
  document.querySelectorAll(".panel").forEach(function (panel) {
    panel.addEventListener("click", function (event) {
      event.stopPropagation();
    });
  });
  var navSortBtns = document.querySelectorAll("nav .sort-btn");
  var navSections = document.querySelectorAll(".panel__all-texts, .panel__collection");
  navSortBtns.forEach(function (sortBtn) {
    sortBtn.addEventListener("click", function () {
      navSortBtns.forEach(function (btn) {
        return btn.classList.remove("active");
      });
      sortBtn.classList.add("active");
      var sections = {
        "sort-btn--all": ".panel__all-texts",
        "sort-btn--years": ".panel__collection--years",
        "sort-btn--categories": ".panel__collection--categories"
      };
      navSections.forEach(function (navSection) {
        return navSection.classList.add("hidden");
      });
      Object.keys(sections).forEach(function (key) {
        if (sortBtn.classList.contains(key)) {
          document.querySelector(sections[key]).classList.remove("hidden");
        }
      });
    });
  });
  openNavBtn.addEventListener("click", function () {
    panelNav.classList.add("panel--visible");
    navOverlay.classList.add("nav-overlay--visible");
    document.body.classList.add("no-scroll");
  });
  closeNavBtn.addEventListener("click", function () {
    closeNav();
  });
  navOverlay.addEventListener("click", function () {
    closeNav();
  });
});