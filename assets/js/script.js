const verticalUnit = getUnit("--unit--vertical");

function getUnit(id) {
  const remFactor = 16;
  const rawUnit =
    getComputedStyle(document.documentElement).getPropertyValue(id) || "1.7rem";
  const remUnit = parseFloat(rawUnit);
  const pxUnit = remUnit * remFactor;
  return pxUnit;
}

// Throttle found here : https://gist.github.com/ionurboz/51b505ee3281cd713747b4a84d69f434
function throttle(func, wait, options) {
  var context, args, result;
  var timeout = null;
  var previous = 0;
  if (!options) options = {};
  var later = function () {
    previous = options.leading === false ? 0 : Date.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function () {
    var now = Date.now();
    if (!previous && options.leading === false) previous = now;
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
}

function setWindowHeightFactor() {
  const windowHeight = window.innerHeight;
  const min = 650;
  const delta = windowHeight - min;
  const factor = roundToNearestHalf(delta / 300) + 1;

  document
    .querySelector(":root")
    .style.setProperty("--window-height-factor", factor);
}

function roundToNearestHalf(num) {
  const round = Math.round(num * 2) / 2;
  return Math.max(round, 0);
}

function toggleLogoState() {
  const scrollY = window.scrollY || window.pageYOffset;

  if (scrollY > 10) {
    document.querySelector("#main-header").classList.add("minimized");
  } else {
    document.querySelector("#main-header").classList.remove("minimized");
  }
}
function toggleFooterState() {
  if (scrollY > 90) {
    document.querySelector(".open-nav-wrapper").classList.remove("hidden");
  } else {
    document.querySelector(".open-nav-wrapper").classList.add("hidden");
  }
}

function fixFootNotes() {
  const footnotes = document.querySelectorAll('a[href^="#sdfootnote"]');

  footnotes.forEach((footnote) => {
    const href = footnote.href;

    footnote.classList.add("footnote");

    if (href.includes("sym")) {
      footnote.id = footnote.hash.replace("sym", "anc").replace("#", "");
    } else if (href.includes("anc")) {
      footnote.id = footnote.hash.replace("anc", "sym").replace("#", "");
    }
  });
}

function removeAccents(str) {
  const from = "áäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
  const to = "aaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }
  return str;
}

function slugify(str) {
  return removeAccents(str.toLowerCase());
}

function subscribe(event) {
  event.preventDefault();
  const emailInput = document.querySelector("#subscribe-form input");

  if (emailInput.value.toLowerCase().match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
    const header = {
      method: "POST",
      body: JSON.stringify(emailInput.value),
    };

    fetch("/subscribe.json", header)
      .then((res) => res.json())
      .then((data) => {
        const formNode = emailInput.parentNode.parentNode;
        formNode.outerHTML = "<p>" + data.message + "</p>";
      });
  } else {
    emailInput.value = "E-mail invalide. Recommencez.";
  }
}

const panelNav = document.querySelector(".panel");
const navOverlay = document.querySelector("#nav-overlay");
const openNavBtns = document.querySelectorAll("button.open-nav");
const closeNavBtn = document.querySelector(".panel-close");
function closeNav() {
  panelNav.classList.remove("panel--visible");
  navOverlay.classList.remove("nav-overlay--visible");
  document.body.classList.remove("no-scroll");
}

document.addEventListener("DOMContentLoaded", () => {
  ragadjust("h1, h2, h4, h5", ["all"]);
  window.window.scrollTo({
    top: 0,
  });

  const handleScroll = throttle(() => {
    toggleLogoState();
    if (window.innerWidth <= 680) {
      toggleFooterState();
    }
  }, 100);
  window.addEventListener("scroll", handleScroll);

  setWindowHeightFactor();
  window.addEventListener("resize", () => {
    setWindowHeightFactor();
  });

  fixFootNotes();

  window.addEventListener("keyup", (event) => {
    if (event.key === "Escape") {
      closeNav();
    }
  });
  document.querySelectorAll(".panel").forEach((panel) => {
    panel.addEventListener("click", (event) => {
      event.stopPropagation();
    });
  });

  const navSortBtns = document.querySelectorAll("nav .sort-btn");
  const navSections = document.querySelectorAll(
    ".panel__all-texts, .panel__collection"
  );

  navSortBtns.forEach((sortBtn) => {
    sortBtn.addEventListener("click", () => {
      navSortBtns.forEach((btn) => btn.classList.remove("active"));
      sortBtn.classList.add("active");

      const sections = {
        "sort-btn--all": ".panel__all-texts",
        "sort-btn--years": ".panel__collection--years",
        "sort-btn--categories": ".panel__collection--categories",
      };

      navSections.forEach((navSection) => navSection.classList.add("hidden"));

      Object.keys(sections).forEach((key) => {
        if (sortBtn.classList.contains(key)) {
          document.querySelector(sections[key]).classList.remove("hidden");
        }
      });
    });
  });

  openNavBtns.forEach((openNavBtn) => {
    openNavBtn.addEventListener("click", () => {
      panelNav.classList.add("panel--visible");
      navOverlay.classList.add("nav-overlay--visible");
      document.body.classList.add("no-scroll");
    });
  });

  closeNavBtn.addEventListener("click", () => {
    closeNav();
  });
  navOverlay.addEventListener("click", () => {
    closeNav();
  });
});
