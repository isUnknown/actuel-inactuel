import gulp from "gulp"
const { watch, parallel, src, dest } = gulp
import cssnano from "gulp-cssnano"
import autoprefixer from "gulp-autoprefixer"
import cssimport from "gulp-cssimport"
import babel from "gulp-babel"

function cssProcess() {
  return src("assets/css/style.css")
    .pipe(cssimport())
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cssnano())
    .pipe(dest("assets/dist"))
}

function jsProcess() {
  return src("assets/js/script.js")
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(dest("assets/dist"))
}

function dev() {
  watch("assets/css/src/*.css", cssProcess)
}

const build = parallel(cssProcess, jsProcess)

export { dev, build }
