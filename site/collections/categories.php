<?php

function createEmptyCategories()
{
    $categories = new Pages();
    foreach (page('textes')->categories()->split() as $categoryName) {
        $category = new Page([
            'slug'     => Str::slug($categoryName),
            'template' => 'category',
            'status'   => 'listed',
            'content'  => [
                'title' => $categoryName,
            ],
            'children' => [],
        ]);

        $categories->add($category);
    }

    return $categories;
}


function createCategories()
{
    $emptyCategories = createEmptyCategories();
    foreach (page('textes')->grandChildren() as $text) {
        try {
            $textCategoryName = $text->category()->value();
            $emptyCategories
                ->findBy('slug', Str::slug($textCategoryName))
                ->children()
                ->add($text);
        } catch (\Throwable $th) {
            throw new Exception($th->getFile() . ' : ' . $th->getMessage());
        }
    }
    return $emptyCategories->filter(function ($category) {
        return $category->children()->count() > 0;
    });
}

return function ($site) {
    $categories = createCategories();
    return $categories;
};
