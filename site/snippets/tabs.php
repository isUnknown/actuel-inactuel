<?php
$left = $left ?? false;

if ($left) {
    $left['slug'] = Str::slug($left['label']);
}
$authorFilter = isset($authorFilter) ? $authorFilter : false;
$activeTab = isset($activeTab) ? Str::slug($activeTab) : '';
?>

<nav id="tabs" 
  x-data="{
    activeTab: ''
  }"
  :class="activeTab.length > 0 ? 'open' : 'close'"
>
    <div class="toggle-btns | flex space-between" style="
    --content:space-between;
    ">
      <?php if ($left): ?>
        <button
          class="toggle left"
          :class="activeTab === '<?=$left['slug'] ?>' ? 'open' : 'close'"
          @click="toggleTab($data, '<?=$left['slug'] ?>')"
        ><?= $left['label'] ?></button>
      <?php endif ?>
      <button
        class="toggle-btn toggle-btn--right"
        :class="activeTab === 'texts' ? 'open' : 'close'"
        title="Voir tous les textes"
        @click="toggleTab($data, 'texts')"
      >catégories</button>
    </div>
    <div 
      class="active-tab"
    >
      <?php if ($left): ?>
        <section 
        x-show="activeTab === '<?=$left['slug'] ?>'">
          <?= $left['content'] ?>
        </section>
      <?php endif ?>
      <ul 
      x-show="activeTab === 'texts'">
      <?php foreach($kirby->collection('years') as $year): ?>
          <div 
            x-data='{
              isOpen: false
            }'
            :class="isOpen ? '' : 'short'"
            class="
              texts__year 
              | full-width 
              flex column 
              bottom"
            style="
              --content: center; 
              --bottom: 2;"
          >
            <h3 class="fs-xl color" style="--color:var(--color-secondary)"><?= $year->title() ?></h3>
            <div class="year__edito">
              <?= $year->edito() ?>
            </div>
            <button 
              :class="isOpen ? 'open' : 'close'"
              class="see-more toggle left" @click="isOpen = !isOpen">Lire</button>
          </div>
        <?php foreach($year->children() as $article): ?>
          <?php if (!$authorFilter || $authorFilter == $article->author()->toPage()->title()->value()) : ?>
            <?php snippet(
                'text-item',
                array(
                                                        'article' => $article
                                                      )
            ) ?>
          <?php endif ?>
        <?php endforeach ?>
      <?php endforeach ?>
      </ul>
    </div>
  </nav>