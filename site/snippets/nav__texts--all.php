<ul class="texts panel__all-texts panel-item-content__texts hidden">
  <?php foreach(page('textes')->grandChildren()->sortBy('published', 'desc') as $article): ?>
  <?php
      $author = $article->author()->toPage() == null ? 'inconnu' : $article->author()->toPage()->title();
      ?>
  <li class="text" x-data="{ 
        title: '<?= str_replace("'", "\'", $article->title()->value()) ?>',
        author: '<?= $author ?>',
        category: '<?= $article->category() ?>',
        keywords: '<?= $article->keywords() ?>',
      }" x-show="
        slugify(title).includes(slugify(search)) 
        || slugify(author).includes(slugify(search))
        || slugify(category).includes(slugify(search))
        || slugify(keywords).includes(slugify(search))
      ">
    <a href="<?= $article->url() ?>"
      class="text__title no-underline">
      <h4><?= $article->title() ?></h4>
      <?php if ($article->subtitle()->isNotEmpty()): ?>
      <p class="text__subtitle"><?= $article->subtitle()->inline() ?></p>
      <?php endif ?>
    </a>
    <div class="text__infos">
      <p>
        <span class="light">par</span>
        <a class="author"
          href="/auteurs/<?= Str::slug($author) ?>"><?= $author ?></a><br>
          
          <span class="light">publié le</span><?php echo $article->published()->toDate('d/m/Y') ?><br>

          <span class="light">dans</span> <a
            href="<?= $article->parent()->url() ?>"><?= $article->parent()->title() ?></a>
          / <a
            href="/categories/<?= Str::slug($article->category()) ?>"><?= $article->category() ?></a>
      </p>
    </div>
  </li>
  <?php endforeach ?>
</ul>