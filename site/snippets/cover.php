<?php
$isOpen ??= false;
?>

<header class="page-cover">
  <div class="title-wrapper">
    <a href="#main-content" class="no-underline" title="aller au texte">
      <?= $slots->title() ?>
    </a>
  </div>
  <?php if ($slots->text()): ?>
  <div class="text-wrapper">
    <?= $slots->text() ?>
  </div>
  <?php endif ?>
  <?php if ($page->isHomePage()): ?>
    <ul class="links">
      <li>
        <a href="/lettre" id="subscribe-btn" class="plus">
          s'inscrire
        </a>
      </li>
      <li>
        <a
          href="<?= $site->find('a-propos')->url() ?>">
          à propos
        </a>
      </li>
    </ul>
  <?php endif ?>
</header>