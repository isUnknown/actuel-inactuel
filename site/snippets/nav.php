<nav class="panel" x-data="{search: ''}">
  <header>
    <p class="sort-btns">
      <button class="sort-btn sort-btn--years active">années</span></button>
      <button class="sort-btn sort-btn--categories">catégories</button>
      <button class="sort-btn sort-btn--all">voir tout</button>
    </p>
    <div class="search">
      <input class="search__input" type="text" placeholder="Chercher" x-model="search">
      <img x-show="search.length === 0"
        src="<?= url('assets/images/icons/search.svg') ?>"
        class="search__icon" alt="Icône loupe indiquant le champ de recherche.">
      <button x-show="search.length > 0" @click="search = ''" class="search__icon">
        <img
          src="<?= url('assets/images/icons/close.svg') ?>"
          alt="">
      </button>
    </div>
  </header>
  <?php snippet('nav__texts--all') ?>
  <?php snippet('nav__texts--collection', ['collection' => 'categories']) ?>
  <?php snippet('nav__texts--collection', ['collection' => 'years']) ?>
  <button class="less panel-close">textes</button>
</nav>
<div id="nav-overlay"></div>