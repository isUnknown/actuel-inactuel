<?php

$entryTopPos ??= 20;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?= $site->title() ?><?= e($page->url() !== $site->url(), ' - ' . $page->title()) ?>
  </title>

  <link rel="stylesheet"
    href="<?= url('assets/css/style.css') . '?version-cache-prevent' . rand(0, 1000) ?>" />

  <script
    src="<?= url('assets') ?>/js/script.js?version-cache-prevent<?= rand(0, 1000)?>"
    defer></script>
  <script defer src="<?= url('assets/js/alpine.min.js') ?>">
  </script>
  <script defer src="<?= url('assets/js/ragadjust.js') ?>">
  </script>

  <meta name="robots" content="noindex, nofollow, noarchive">

  <?php if ($page->template() == 'linear' || $page->template() == 'grid'): ?>
  <!-- SCHEMA -->
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "headline": "<?= $page->title() ?>",
      "author": [{
        "@type": "Person",
        "name": "<?= $page->author() ?>"
      }],
      "image": "<?= url('assets/images/logo.png') ?>",
      "description": "<?= $page->title() ?>, par <?= $page->author()->toPage()->title() ?>. Publié le <?= $page->published()->toDate('d/m/Y') ?> dans <?= $page->parent()->title() ?>/<?= $page->category() ?>",
      "datePublished": "<?= $page->published()->toDate('Y-m-d') ?>",
      "dateModified": "<?= $page->modified('Y-m-d') ?>",
      "url": "<?= $page->url() ?>",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "<?= $page->url() ?>"
      },
      "publisher": {
        "@type": "Organization",
        "name": "actuel - inactuel"
      }
    }
  </script>
  <?php endif ?>

  <!-- FAVICON -->
  <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
  <link rel="shortcut icon" href="/favicon.ico" />
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
  <meta name="apple-mobile-web-app-title" content="actuel-inactuel" />
  <link rel="manifest" href="/site.webmanifest" />

  <?php if ($page->additionnalCss()->isNotEmpty()): ?>
    <!-- CUSTOM CSS -->
    <style>
      <?= $page->additionnalCss() ?>
    </style>
      
  <?php endif ?>
</head>

<body
  class="background-grid <?= e($page->fullWidth() == 'true', 'full-width') ?>"
  data-template="<?= $page->template() ?>">
  <header id="main-header">
    <a id="logo"
      href="<?= e($page->isHomePage(), '#main-edito', $site->url()) ?>"
      class="no-underline"
      title="<?= e($page->isHomePage(), 'lire l\'éditorial', 'aller à l\'accueil') ?>">
      <h1>
        <span id="actuel">actuel</span>
        <span id="inactuel">inactuel</span>
      </h1>
    </a>
  </header>
  <?php snippet('nav') ?>