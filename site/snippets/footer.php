
<?php if (!$page->is(page('lettre')) && !$page->is(page('a-propos'))): ?>
  <footer id="main-footer">
    <ul id="links">
      <li class="open-nav-wrapper hidden">
        <button class="plus open-nav" title="chercher parmi les textes">textes</button>
      </li>
      <li>
        <a href="/lettre" id="subscribe-btn" class="plus">
          s'inscrire
        </a>
      </li>
      <li>
        <a
          href="<?= $site->find('a-propos')->url() ?>">
          à propos
        </a>
      </li>
    </ul>
  </footer>
  <?php endif ?>
</body>

</html>