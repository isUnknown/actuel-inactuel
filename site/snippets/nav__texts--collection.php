<ul
  class="panel__collection panel__collection--<?= $collection ?><?= e($collection == 'categories', ' hidden') ?>">
  <?php foreach($kirby->collection($collection) as $section): ?>
  <li class="panel__item " x-data='{ isOpen: false }'>
    <a class="no-underline" href="#<?= $section->slug() ?>"
      id="<?= $section->slug() ?>">
      <button class="panel__toggle-btn" @click="isOpen = !isOpen">
        <h3><?= $section->title() ?></h3>
        <div class="panel__toggle-icon" x-text="isOpen || search.length > 0 ? '-' : '+'"></div>
      </button>
    </a>
    <div class="panel-item-content" x-show="isOpen || search.length > 0" x-data='{ edito: false }'>
      <?php if ($collection === 'years'): ?>
      <div class="panel-item-content__edito" :class="edito ? '' : 'short'" x-show="search.length === 0">
        <?= $section->edito() ?>
      </div>
      <button :class="edito ? 'open' : 'close'" x-show="search.length === 0"
        class="see-more toggle-btn toggle-btn--left" @click="edito = !edito">Lire</button>
      <?php endif ?>
      <ul class="panel-item-content__texts texts">
        <?php foreach($section->children() as $article): ?>
        <?php
            $author = $article->author()->toPage() == null ? 'inconnu' : $article->author()->toPage()->title();
            ?>
        <li class="text" x-data="{ 
              title: '<?= str_replace("'", "\'", $article->title()->value()) ?>',
              author: '<?= $author ?>',
              category: '<?= $article->category() ?>',
              keywords: '<?= $article->keywords() ?>',
            }" x-show="
              slugify(title).includes(slugify(search)) 
              || slugify(author).includes(slugify(search))
              || slugify(category).includes(slugify(search))
              || slugify(keywords).includes(slugify(search))
            ">
          <a href="<?= $article->url() ?>"
            class="text__title no-underline">
            <h4><?= $article->title() ?></h4>
            <?php if ($article->subtitle()->isNotEmpty()): ?>
            <p class="text__subtitle"><?= $article->subtitle()->inline() ?></p>
            <?php endif ?>
          </a>
          <div class="text__infos">
            <p>
              <span class="light">par</span>
              <a class="author"
                href="/auteurs/<?= Str::slug($author) ?>"><?= $author ?></a><br>
              
                <span class="light">publié le</span><?php echo $article->published()->toDate('d/m/Y') ?><br>
              
                <span class="light">dans</span> <a
                href="<?= $article->parent()->url() ?>"><?= $article->parent()->title() ?></a>
              / <a
                href="/categories/<?= Str::slug($article->category()) ?>"><?= $article->category() ?></a>
            </p>
          </div>
        </li>
        <?php endforeach ?>
      </ul>
    </div>
  </li>
  <?php endforeach ?>
</ul>