<li 
  class="text"
>
  <a href="<?= $article->url() ?>" class="text__title no-underline">
    <h3><?= $article->title() ?></h3>
  </a>
  <div class="text__infos">
    <p>
        <span class="light">par</span>
        <a class="author" href="/auteurs/<?= Str::slug($article->author()->toPage()->title()) ?>"><?= $article->author()->toPage()->title() ?></a><br>
        <span class="light">publié le </span><?= $article->published()->toDate('d/m/Y') ?><br>
        <span class="light">dans</span> <a href="<?= $article->parent()->url() ?>"><?= $article->parent()->title() ?></a> / <a href="/categories/<?= $article->category() ?>"><?= $article->category() ?></a>
    </p>
  </div>
</li>