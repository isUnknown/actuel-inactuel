<?php snippet('header') ?>

<main>
  <article>
    <?php snippet('cover', slots: true) ?>
      <?php slot('title') ?>
      <a 
        href="#main-edito" 
        class="no-underline home-baseline" 
        title="lire l'éditorial"
      >
        <h2 class="main-title <?= setTitleFontSizeClass($site->subtitle()) ?>"><?= $site->subtitle()->inline() ?></h2>
      </a>
      <a href="#main-edito" class="main-edito-btn | no-underline" title="lire l'éditorial">
        <p 
          class="oggle-btn toggle-btn--left"
        >éditorial</p>
      </a>
      <button class="plus open-nav" title="ouvrir la navigation">textes</button>
      <?php endslot() ?>
    <?php endsnippet() ?>
    <div id="main-edito" id="main-content">
      <?= $site->edito() ?>
    </div>
  </article>
</main>
<?php snippet('footer') ?>