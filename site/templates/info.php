<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', ['isOpen' => true], slots: true) ?>
    <?php slot('title') ?>
    <h2
      class="main-title <?= setTitleFontSizeClass($page->title()) ?>">
      <?= $page->title() ?></h2>
      <div id="main-content">
        <?= $page->body() ?>
      </div>
    <?php endslot() ?>
    <?php endsnippet() ?>

  </article>
</main>

<?php snippet('footer') ?>