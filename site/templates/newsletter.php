<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', array('isOpen' => true), slots: true) ?>
      <?php slot('title') ?>
      <h2 class="main-title <?= setTitleFontSizeClass($page->title()) ?>"><?= $page->title() ?></h2>
      <?= $page->body() ?>
      <form id="subscribe-form">
        <label for="email">
          <input type="email" name="email" id="email" placeholder="votre e-mail">
          <button type="submit" onclick="subscribe(event)">→</button>
        </label>
      </form>
      <?php endslot() ?>
    <?php endsnippet() ?>
  </article>
</main>

<?php snippet('footer') ?>