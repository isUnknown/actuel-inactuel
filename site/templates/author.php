<?php snippet('header'); ?>

<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', slots: true) ?>
      <?php slot('title') ?>
        <h2 class="main-title <?= setTitleFontSizeClass($page->title()) ?>"><?= $page->title() ?></h2>
        <?= $page->presentation() ?>
      <?php endslot() ?>
    <?php endsnippet() ?>
    <div id="main-content">
      <p class="h3">textes :</p>
        <ul class="texts">
      <?php foreach(page('textes')->grandChildren() as $article): ?>
          <?php 
            if ($article->author()->toPage()->title()->value() == $page->title()->value()): ?>
            <li 
              class="text"
            >
              <a href="<?= $article->url() ?>" class="text__title no-underline">
                <h4><?= $article->title() ?></h4>
              </a>
              <div class="text__infos">
                <p>
                    <span class="light">publié le </span><?= $article->published()->toDate('d/m/Y') ?><br>
                    <span class="light">dans</span> <a href="<?= $article->parent()->url() ?>"><?= $article->parent()->title() ?></a> / <a href="/categories/<?= $article->category() ?>"><?= $article->category() ?></a>
                </p>
              </div>
            </li>
          <?php endif ?>
      <?php endforeach ?>
      </ul>
    </div>
  </article>
</main>