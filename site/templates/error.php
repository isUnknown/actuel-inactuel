<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', ['isOpen' => true], slots: true) ?>
    <?php slot('title') ?>
    <h2
      class="main-title fs-xl">
      <?= $page->title() ?></h2>
    <?php endslot() ?>
    <?php endsnippet() ?>
    <div id="main-content">
      <?= $page->body() ?>
      <p><a href="/">Retour à l'accueil</a></p>
    </div>
  </article>
</main>

<?php snippet('footer') ?>