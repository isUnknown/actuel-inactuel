<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', array('isOpen' => true), slots: true) ?>
      <?php slot('title') ?>
      <h1 class="main-title"><?= $page->title() ?></h1>
      
      <?php if ($page->published()->isNotEmpty()): ?>
        <p>
          <span class="light">courrier du </span><?= $page->published()->toDate('d/m/Y') ?>
        </p>
      <?php endif ?>
      
      <?php endslot() ?>
    <?php endsnippet() ?>
    
    <div id="main-content">
      <?= $page->body() ?>
    </div>
  </article>
</main>

<?php snippet('footer') ?>