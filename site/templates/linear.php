<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', ['isOpen' => true], slots: true) ?>
    <?php slot('title') ?>
    <h1 class="main-title"><?= $page->title() ?></h1>
    <?php if ($page->subtitle()->isNotEmpty()): ?>
    <h2 class="main-subtitle fs-l"><?= $page->subtitle()->inline() ?>
    </h2>
    <?php endif ?>
    <p>
      <span class="light">par</span>
      <a class="author"
        href="/auteurs/<?= Str::slug($page->author()->toPage()->title()) ?>"><?= $page->author()->toPage()->title() ?></a><br>
      <span class="light">publié le
      </span><?= $page->published()->toDate('d/m/Y') ?><br>
      <span class="light">dans</span> <a
        href="<?= $page->parent()->url() ?>"
        title="voir les textes liés à l'année <?= $page->parent()->title() ?>"><?= $page->parent()->title() ?></a>
      / <a href="/categories/<?= Str::slug($page->category()) ?>"
        title="voir les textes de la catégorie <?= $page->category() ?>"><?= $page->category() ?></a>
    </p>
    <?php endslot() ?>
    <?php endsnippet() ?>

    <div id="main-content">
      <?php if ($page->chapo()->isNotEmpty()): ?>
      <div id="chapo">
        <?= $page->chapo() ?>
      </div>
      <?php endif ?>
      <?php if ($page->isHtmlMode()->isTrue()): ?>
      <?= $page->htmlBody()->kt() ?>
      <?php else: ?>
      <?= $page->body() ?>
      <?php endif ?>
    </div>
  </article>
</main>

<?php snippet('footer') ?>