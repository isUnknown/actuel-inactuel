<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <h1 class="main-title"><?= $page->title() ?></h1>

    <div id="main-content" x-data="{
        edito: false
      }">
      <?php if ($page->edito()->isNotEmpty()): ?>
      <div id="edito" :class="edito ? '' : 'short'">
        <?= $page->edito() ?>
      </div>
      <button :class="edito ? 'open' : 'close'" class="see-more toggle-btn toggle-btn--left"
        @click="edito = !edito">Lire</button>
      <?php endif ?>
      <ul class="texts">
        <?php foreach($page->texts()->toPages() as $article): ?>
        <li class="text">
          <a href="<?= $article->url() ?>"
            class="text__title no-underline">
            <h4><?= $article->title() ?></h4>
          </a>
          <div class="text__infos">
            <p>
              <span class="light">par</span>
              <a class="author"
                href="/auteurs/<?= Str::slug($article->author()->toPage()->title()) ?>"><?= $article->author()->toPage()->title() ?></a><br>
              <span class="light">publié le
              </span><?= $article->published()->toDate('d/m/Y') ?><br>
              <span class="light">dans</span> <a
                href="<?= $article->parent()->url() ?>"><?= $article->parent()->title() ?></a>
              / <a
                href="/categories/<?= $article->category() ?>"><?= $article->category() ?></a>
            </p>
          </div>
        </li>
        <?php endforeach ?>
    </div>
  </article>
</main>

<?php snippet('footer') ?>