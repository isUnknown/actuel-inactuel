<?php snippet('header') ?>
<main id="<?= $page->template() ?>">
  <article>
    <?php snippet('cover', ['isOpen' => true], slots: true) ?>
    <?php slot('title') ?>
    <h2
      class="main-title <?= setTitleFontSizeClass($page->title()) ?>">
      <?= $page->title() ?></h2>
    <p>
      <span class="light">par</span>
      <a class="author"
        href="/auteurs/<?= Str::slug($page->author()->toPage()->title()) ?>"><?= $page->author()->toPage()->title() ?></a><br>
      <span class="light">publié le
      </span><?= $page->published()->toDate('d/m/Y') ?><br>
      <span class="light">dans</span> <a
        href="<?= $page->parent()->url() ?>"><?= $page->parent()->title() ?></a>
      / <a
        href="/categories/<?= $page->category() ?>"><?= $page->category() ?></a>
    </p>
    <?php endslot() ?>
    <?php endsnippet() ?>
    
    <div id="main-content">

      <?php if ($page->chapo()->isNotEmpty()): ?>
      <div id="chapo">
        <?= $page->chapo() ?>
      </div>
      <?php endif ?>

      <?php foreach ($page->body()->toLayouts() as $layout): ?>
      <section class="grid" id="<?= $layout->id() ?>"
        data-columns="<?= $layout->columns()->count() ?>">
        <?php foreach ($layout->columns() as $column): ?>
        <div class="column" style="--span:<?= $column->span(32) ?>">
          <div class="blocks">
            <?= $column->blocks() ?>
          </div>
        </div>
        <?php endforeach ?>
      </section>
      <?php endforeach ?>
    
    </div>
  </article>
</main>

<?php snippet('footer') ?>