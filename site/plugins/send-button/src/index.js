import SendButtonField from "./components/SendButtonField.vue";

window.panel.plugin("adrienpayet/send-button", {
  fields: {
    "send-button": SendButtonField,
  },
});
