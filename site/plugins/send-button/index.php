<?php

Kirby::plugin('adrienpayet/send-button', [
  'fields' => [
    'send-button' => [
      'computed' => [
        'pageUri' => function () {
            return $this->model()->uri();
        },
        'pageStatus' => function () {
            return $this->model()->status();
        },
      ],
    ],
  ],
  'routes' => [
    require __DIR__ . '/routes/send-newsletter.php',
    require __DIR__ . '/routes/unsubscribe-newsletter.php',
  ],
]);
