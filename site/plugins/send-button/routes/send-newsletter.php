<?php

use MailerSend\MailerSend;
use MailerSend\Helpers\Builder\Recipient;
use MailerSend\Helpers\Builder\EmailParams;

set_time_limit(600);

return [
    'pattern' => '/send-newsletter.json',
    'method'  => 'POST',
    'action'  => function () {
        $jsonRequest = file_get_contents('php://input');
        $data = json_decode($jsonRequest);

        if (!$data || !isset($data->pageUri) || !isset($data->isTest)) {
            return json_encode([
                'status'  => 'error',
                'message' => 'Invalid request data. Required fields: pageUri, isTest.',
            ]);
        }

        $kirby = kirby();
        $emailPage = page('lettre')->childrenAndDrafts()->find($data->pageUri);

        if (!$emailPage) {
            return json_encode([
                'status'  => 'error',
                'message' => 'The specified page does not exist.',
            ]);
        }

        $recipients = $data->isTest
            ? $emailPage->testAdressList()->toStructure()->pluck('email', ',', true)
            : page('lettre')->subscribers()->toStructure()->pluck('email', ',', true);

        if (empty($recipients)) {
            return json_encode([
                'status'  => 'error',
                'message' => 'No recipients found.',
            ]);
        }

        $subject = $data->isTest
            ? '[TEST] - ' . $emailPage->title()->value()
            : $emailPage->title()->value();

        $mailersend = new MailerSend(['api_key' => kirby()->option('mailerSendApiKey')]);
        $bulkEmailParams = [];

        foreach ($recipients as $recipient) {
            $bulkEmailParams[] = (new EmailParams())
                ->setFrom('info@actuel-inactuel.fr')
                ->setFromName('actuel-inactuel')
                ->setRecipients([new Recipient($recipient, 'abonné')])
                ->setSubject($subject)
                ->setHtml($emailPage->body())
                ->setReplyTo('info@actuel-inactuel.fr')
                ->setReplyToName('actuel-inactuel');
        }

        try {
            $response = $mailersend->bulkEmail->send($bulkEmailParams);
            file_put_contents($emailPage->root() . '/mailersend_log.json', json_encode($response, JSON_PRETTY_PRINT));
            return json_encode([
                'status'  => 'success',
                'message' => 'All emails sent successfully.',
            ]);
        } catch (\Exception $e) {
            $date = Kirby\Toolkit\Date::now(new DateTimeZone('Europe/Paris'));
            file_put_contents(
                $emailPage->root() . '/mailersend_errors.log',
                $date->format('d/m/Y H:i:s') . ' ' . $e->getMessage() . PHP_EOL,
                FILE_APPEND
            );
            return json_encode([
                'status'  => 'error',
                'message' => 'Erreur enregistrée dans le log : ' . $e->getMessage(),
            ]);
        }

        if (!$data->isTest) {
            $emailPage->update([
                'published' => Kirby\Toolkit\Date::Today(),
            ]);
            $emailPage->changeStatus('listed');
        }
    },
];
