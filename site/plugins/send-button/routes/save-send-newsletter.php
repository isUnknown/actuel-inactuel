<?php


set_time_limit(600);

return [
    'pattern' => '/send-newsletter.json',
    'method'  => 'POST',
    'action'  => function () {
        $jsonRequest = file_get_contents('php://input');
        $data = json_decode($jsonRequest);

        if (!$data || !isset($data->pageUri) || !isset($data->isTest)) {
            return json_encode([
                'status'  => 'error',
                'message' => 'Invalid request data. Required fields: pageUri, isTest.',
            ]);
        }

        $kirby = kirby();
        $emailPage = page('lettre')->childrenAndDrafts()->find($data->pageUri);

        if (!$emailPage) {
            return json_encode([
                'status'  => 'error',
                'message' => 'The specified page does not exist.',
            ]);
        }

        $recipients = $data->isTest
            ? $kirby->users()->pluck('email', null, true)
            : page('lettre')->subscribers()->toStructure()->pluck('email', ',', true);

        if (empty($recipients)) {
            return json_encode([
                'status'  => 'error',
                'message' => 'No recipients found.',
            ]);
        }

        $subject = $data->isTest
            ? '[TEST] - ' . $emailPage->title()->value()
            : $emailPage->title()->value();

        $from = new \Kirby\Cms\User([
            'email' => 'info@actuel-inactuel.fr',
            'name'  => 'actuel-inactuel',
          ]);

        $sentEmails = [];
        $errors = [];

        $batchSize = 5;
        $pauseBetweenBatches = 1;

        foreach (array_chunk($recipients, $batchSize) as $batch) {
            foreach ($batch as $recipient) {
                try {
                    $kirby->email([
                        'from'     => $from,
                        'to'       => $recipient,
                        'subject'  => $subject,
                        'template' => 'newsletter',
                        'data'     => [
                            'body'      => $emailPage->body(),
                            'url'       => (string) $emailPage->url(),
                            'recipient' => $recipient,
                        ],
                    ]);
                    $sentEmails[] = $recipient;
                } catch (Exception $error) {
                    $errors[] = [
                        'email'   => $recipient,
                        'message' => $error->getMessage(),
                    ];
                }
            }

            sleep($pauseBetweenBatches);
        }

        if (!$data->isTest) {
            $emailPage->changeStatus('listed');
        } else {
            $emailPage->changeStatus('unlisted');
        }

        return json_encode([
            'status'  => empty($errors) ? 'success' : 'partial',
            'message' => empty($errors) ? 'All emails sent successfully.' : 'Certains emails n\'ont pas pu être envoyé.',
            'sent'    => $sentEmails,
            'failed'  => $errors,
        ]);
    },
];
