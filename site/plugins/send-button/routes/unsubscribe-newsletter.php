<?php

return [
    'pattern' => '/desinscription',
    'action'  => function () {
        $kirby = kirby();
        $kirby->impersonate('kirby');

        $email = get('email');
        $page = page('lettre');

        $emailExists = in_array(['email' => $email], $page->subscribers()->yaml());

        if (!$emailExists) {
            $title = 'Erreur';
            $body = '<p>' . $email . ' est introuvable dans la base de données. Vous pouvez demander une désinscription manuelle en écrivant à <a href="mailto:info@actuel-inactuel.fr?subject=Demande de désinscription&body=Bonjour,%0D%0AJe souhaite être désinscrit d\'actuel-inactuel.%0D%0AE-mail : ' . $email . '%0D%0AMerci.%0D%0A"">info@actuel-inactuel.fr</a>.</p>';
        } else {
            $subscribers = array_filter(
                $page->subscribers()->yaml(),
                fn($subscriber) => $subscriber !== ['email' => $email]
            );

            $page->update([
                'subscribers' => $subscribers,
            ]);

            $title = $email . ' désinscrit';
            $body = '<p>Si la désinscription automatique n\'a pas fonctionné et que vous continuez à recevoir des mails, vous pouvez demander une désinscription manuelle en écrivant à <a href="mailto:info@actuel-inactuel.fr?subject=Demande de désinscription&body=Bonjour,%0D%0AJe souhaite être désinscrit d\'actuel-inactuel.%0D%0AE-mail : ' . $email . '%0D%0AMerci.%0D%0A"">info@actuel-inactuel.fr</a>.<p>';

        }

        return new Page([
            'slug'     => Str::slug('desinscription'),
            'template' => 'error',
            'status'   => 'unlisted',
            'content'  => [
                'title' => $title,
                'body'  => $body,
            ],
        ]);
    },
];
