(function() {
  "use strict";
  function normalizeComponent(scriptExports, render, staticRenderFns, functionalTemplate, injectStyles, scopeId, moduleIdentifier, shadowMode) {
    var options = typeof scriptExports === "function" ? scriptExports.options : scriptExports;
    {
      options.render = render;
      options.staticRenderFns = staticRenderFns;
      options._compiled = true;
    }
    {
      options._scopeId = "data-v-" + scopeId;
    }
    return {
      exports: scriptExports,
      options
    };
  }
  const _sfc_main = {
    __name: "SendButtonField",
    props: {
      pageUri: String,
      pageStatus: String
    },
    setup(__props) {
      const { pageUri } = __props;
      const testBtnState = Vue.ref({
        icon: "plane",
        text: "Tester",
        theme: null,
        error: null
      });
      const sendBtnState = Vue.ref({
        icon: "plane",
        text: "Envoyer",
        theme: "orange",
        error: null
      });
      async function send(event, isTest = false) {
        event.preventDefault();
        const currentBtnState = isTest ? testBtnState : sendBtnState;
        currentBtnState.value = {
          theme: "blue",
          text: "En cours…",
          icon: "loader",
          error: null
        };
        const headers = {
          method: "POST",
          body: JSON.stringify({
            pageUri,
            isTest
          })
        };
        try {
          const response = await fetch("/send-newsletter.json", headers);
          const data = await response.json();
          console.log(data);
          if (data.status === "success") {
            currentBtnState.value = {
              theme: "green",
              icon: "check",
              text: isTest ? "Test envoyé" : "Envoi réussi",
              error: null
            };
            setTimeout(() => {
              location.href = location.href;
            }, 2e3);
          } else {
            let errorMessage = data.message || "Erreur inconnue";
            throw new Error(errorMessage);
          }
        } catch (error) {
          currentBtnState.value = {
            theme: "red",
            icon: "alert",
            text: isTest ? "Échec du test" : "Erreur",
            error: error.message
          };
        }
      }
      return { __sfc: true, testBtnState, sendBtnState, send };
    }
  };
  var _sfc_render = function render() {
    var _vm = this, _c = _vm._self._c, _setup = _vm._self._setupProxy;
    return _vm.pageStatus !== "listed" ? _c("div", { staticClass: "wrapper" }, [_c("div", { staticClass: "test-wrapper" }, [_c("k-button", { staticStyle: { "width": "max-content" }, attrs: { "variant": "filled", "help": "test", "theme": _setup.testBtnState.theme, "icon": _setup.testBtnState.icon }, on: { "click": function($event) {
      return _setup.send($event, true);
    } } }, [_vm._v(_vm._s(_setup.testBtnState.text))]), _vm._m(0), _setup.testBtnState.error ? _c("k-box", { staticStyle: { "margin-top": "var(--spacing-1)" }, attrs: { "theme": "error" } }, [_vm._v(_vm._s(_setup.testBtnState.error))]) : _vm._e()], 1), _c("div", { staticClass: "send-wrapper" }, [_c("k-button", { attrs: { "variant": "filled", "theme": _setup.sendBtnState.theme, "icon": _setup.sendBtnState.icon }, on: { "click": function($event) {
      return _setup.send($event);
    } } }, [_vm._v(_vm._s(_setup.sendBtnState.text))]), _setup.sendBtnState.error ? _c("k-box", { staticStyle: { "margin-top": "var(--spacing-1)" }, attrs: { "theme": "error" } }, [_vm._v(_vm._s(_setup.sendBtnState.error))]) : _vm._e(), _vm._m(1)], 1)]) : _vm._e();
  };
  var _sfc_staticRenderFns = [function() {
    var _vm = this, _c = _vm._self._c;
    _vm._self._setupProxy;
    return _c("footer", { staticClass: "k-field-footer" }, [_c("div", { staticClass: "k-help k-field-help k-text" }, [_c("p", [_vm._v("Envoie seulement à la liste ci-dessus")])])]);
  }, function() {
    var _vm = this, _c = _vm._self._c;
    _vm._self._setupProxy;
    return _c("footer", { staticClass: "k-field-footer" }, [_c("div", { staticClass: "k-help k-field-help k-text" }, [_c("strong", [_vm._v("⚠ Envoie à tous les abonnés.")])])]);
  }];
  _sfc_render._withStripped = true;
  var __component__ = /* @__PURE__ */ normalizeComponent(
    _sfc_main,
    _sfc_render,
    _sfc_staticRenderFns,
    false,
    null,
    "4847e765"
  );
  __component__.options.__file = "/Users/adrienpayet/code/actuel-inactuel/site/plugins/send-button/src/components/SendButtonField.vue";
  const SendButtonField = __component__.exports;
  window.panel.plugin("adrienpayet/send-button", {
    fields: {
      "send-button": SendButtonField
    }
  });
})();
