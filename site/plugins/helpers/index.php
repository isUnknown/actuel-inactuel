<?php

function setTitleFontSizeClass($title, $level = 'h1')
{
    $length = strlen($title);

    if ($level === 'h1') {
        switch (true) {
            case ($length < 35):
                return 'fs-xxl';
                break;

            case ($length < 70):
                return 'fs-xl';
                break;

            default:
                return 'fs-l';
                break;
        }
    }
}

function getAuthorBySlug($slug)
{
    $site = site();
    $authors = page("auteurs")->children();
    $author = $authors->find($slug);

    return $author;
}
 