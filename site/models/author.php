<?php
class AuthorPage extends Page {
  public function getTexts() {
    return page('textes')->index()->filter(function($text) {
      return $text->author()->toPage() === $this;
    });
  }
}