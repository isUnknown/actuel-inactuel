<?php

return function ($page) {
    if ($page->template() == 'email') {
        $list = [
          ['email' => 'fournelcecile@yahoo.fr'],
          ['email' => 'payet.adrien@protonmail.com'],
          ['email' => 'mazet.zaccardelli@free.fr'],
          ['email' => 'wafaabida@hotmail.com'],
          ['email' => 'elisegarraud@yahoo.fr'],
          ['email' => 'pierre-damien.huyghe@univ-paris1.fr'],
        ];
        $page->update([
          'testAdressList' => Yaml::encode($list),
        ]);
    }
};
