<?php

return [
  'site' => [
    'label'   => 'Accueil',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'site');
    },
  ],
  'texts' => [
    'icon'    => 'pen',
    'label'   => 'Textes',
    'link'    => 'pages/textes',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/textes');
    },
  ],
  'authors' => [
    'icon'    => 'users',
    'label'   => 'Auteurs',
    'link'    => 'pages/auteurs',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/auteurs');
    },
  ],
  '-',
  '-',
  'infos' => [
    'icon'    => 'question',
    'label'   => 'À propos',
    'link'    => 'pages/a-propos',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/a-propos');
    },
  ],
  'subscription' => [
    'icon'    => 'email',
    'label'   => 'Liste de diffusion',
    'link'    => 'pages/lettre',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/liste-de-diffusion');
    },
  ],
  '-',
  '-',
  'users',
  'comments',
  'admin' => [
    'icon'    => 'folder',
    'label'   => 'Administration',
    'link'    => 'pages/admin',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/admin');
    },
  ],
  '-',
  '-',
  'system',
];
