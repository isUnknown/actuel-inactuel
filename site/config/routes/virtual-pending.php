<?php

use Kirby\Uuid\Uuid;

return [
  'pattern' => 'a-venir',
  'action'  => function () {
      return Page::factory([
          'slug'     => 'a-venir',
          'template' => 'error',
          'content'  => [
              'title' => 'Textes disponibles à partir du 21 janvier',
              'body'  => '<p>Rendez-vous vendredi 21 février à partir de 18h, au Centre Césure (ancien campus Censier), 13 rue Santeuil, 75005 Paris - salle Las Vergnas (métro Censier-Daubenton ou Saint-Marcel).</p>',
              'uuid'  => Uuid::generate(),
          ],
      ]);
  },
];
