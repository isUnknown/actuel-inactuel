<?php

use Kirby\Uuid\Uuid;

return [
  'pattern' => 'categories/(:any)',
  'action'  => function ($category) {

      $allTexts = page('textes')->grandChildren();
      $textsInCategory = $allTexts->filter(
          fn($text) => Str::slug($text->category()) === $category
      );

      $texts = [];

      foreach ($textsInCategory as $text) {
          $texts[] = (string) $text->uri();
      }

      try {
          $title = $textsInCategory->first()->category();
      } catch (\Throwable $th) {
          go('/erreur');
      }

      return Page::factory([
          'slug'     => Str::slug($category) . '-' . Uuid::generate(),
          'template' => 'category',
          'model'    => 'category',
          'content'  => [
              'title' => $title,
              'texts' => Yaml::encode($texts),
              'uuid'  => Uuid::generate(),
          ],
      ]);
  },
];
