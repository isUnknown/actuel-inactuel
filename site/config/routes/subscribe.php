<?php

return [
  'pattern' => '/subscribe.json',
  'method'  => 'POST',
  'action'  => function () {
      $jsonRequest = file_get_contents('php://input');
      $email = Str::lower(json_decode($jsonRequest));

      if (V::email($email)) {
          kirby()->impersonate('kirby');

          $page = page('lettre');
          $subscribers = $page->subscribers()->yaml();

          $emailExists = in_array(['email' => $email], $subscribers);

          if ($emailExists) {
              return [
                  'status'  => 'error',
                  'message' => 'Cet email est déjà inscris.',
              ];
          }

          $newSubscriber = ['email' => $email];
          $subscribers[] = $newSubscriber;

          $page->update([
              'subscribers' => $subscribers,
          ]);

          return [
              'status'  => 'success',
              'message' => 'lettre réussie.',
          ];
      } else {
          return [
              'status'  => 'error',
              'message' => 'Email invalide.',
          ];
      }
  },
];
